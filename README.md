docker container run -it --name terraform_fargate debian:stretch bash

apt-get update && apt-get install -y python3 git curl wget unzip bash-completion vim awscli groff

wget https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip
unzip terraform_0.11.13_linux_amd64.zip
mv terraform /usr/local/bin/

update-alternatives --install /usr/bin/python python /usr/bin/python3.5 1

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py

pip install ansible

aws configure

git clone https://gitlab.com/mvicha/terraform_fargate.git
cd terraform_fargate

terraform init
terraform plan --out=plan.out
terraform apply plan.out
