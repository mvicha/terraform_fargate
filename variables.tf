variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "us-east-1"
}

variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}

variable "app_image" {
  description = "Docker image to run in the ECS cluster"
  default     = "mvilla/customindex:latest"
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 80
}

variable "app_count" {
  description = "Number of docker containers to run"
  default     = 2
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "256"
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "512"
}

variable "clustername" {
  description = "Cluster Name"
  default     = "mvil-fargate-cluster"
}

variable "loadbalancer_name" {
  description = "Load Balancer Name"
  default     = "mvil-fargate-alb"
}

variable "alb-sg-name" {
  description = "Load Balancer Security Group name"
  default     = "mvil-alb-sg"
}

variable "traffic-sg-name" {
  description = "Traffic Security Group name"
  default     = "mvil-traffic-sg"
}
